# unicorn_stories_app

This alpha and this is a placeholder name. General idea is I do not have time and other resources to fully recreate the User Case BDD soltion website
Storybook that FB React Native devs use. But, I do have time to quickly hack up a smal mobile version and put it through its paces.

## Overview

We do not have anything dart native for BDD. If i can declarativve describe behaviors of each screen than I can use flutter driver 
to do real BDD testing as part of the declarative UI coding process which than of course integrates real well with the visual design 
team mock up UI sprints. In short words, way to fly real fast developing UI components in isolation, the new coool kid kick and latest 
treand in UI design best practices.

Let's freaking rock!

## Planning

First, I have to put in integraation with the platform widgets plugin:

[flutter platform widgets](https://github.com/aqwert/flutter_platform_widgets)

as its easier than doing the Google Platform design dynamic delivery of material and cupertino widgets to 
the android and ios platforms. If I need to do another variant for those that hand roll their own 
solution than I will cross that bridge when I come to it.

Second, add BMW Tech's Ozzie:

[BMW Tech Ozzie](https://github.com/bmw-tech/ozzie.flutter)

for screen captures during testing.

Third, take my build_envs_app example:

[build_envs_app](https://gitlab.com/fred.grott/build_envs_app)

and make it best practie worthy of keeping envs out fo the git repo.

Forth, do a naked without dynamic material to android and cupterino to ios widgets solution so that 
I can polish up the orginal sotryboard solution and correct some major problems with it.

Fifth, than figure out rest of the flutter platform wigets solution of doing things 
and implement it.

Sixth, implement some real good examples fully integrated with flutter driver.

Seven, How do I run individual stories? Create a dynamic screens with just one 
list item of that story and than have app execute each sctoryboard? Need to look 
at it a different way.


# Resources

The naked one without the flutter platform widgets Patform Design delivery of 
material to android and cupertino to ios is at:

[dargon-stories_app]()



# Credits

Mike Hoolehan(ilikerobots) came up with a small brief thing called

[Storyboard](https://github.com/ilikerobots/storyboard)

I just took the same idea and extreemly polished it up and geared it up to add more bells and whistles.

# License

BSD Clause 2 copyhright 2019 Fredrick grott